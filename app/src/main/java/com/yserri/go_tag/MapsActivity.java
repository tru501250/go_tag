package com.yserri.go_tag;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;
import android.widget.TextView;
import android.app.AlertDialog.Builder;
import android.content.Intent;
import android.location.GpsStatus.Listener;
import android.os.SystemClock;
import android.view.View;
import android.view.View.OnClickListener;
import java.util.List;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;


public class MapsActivity extends AppCompatActivity
{
    private GoogleMap mMap;

    protected static boolean AllertAlreadyShown = false;
    String Acuracy = "";
    int Col;
    Address CurrentAddress;
    Location CurrentLocation;
    boolean GPSsignal = true;
    GpsStatus.Listener GPSstatusLSNR;
    LocationListener LocationLSNR;
    String OutText = "";
    boolean WeakSignal = false;
    TextView button;
    EditText coords;
    Criteria criteria;
    Geocoder geocoder;
    boolean isFirstLock = false;
    LocationManager lm;
    String lp;
    long mLastLocationMillis;
    TextView prov;

    private void setUpMapIfNeeded()
    {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null)
        {
            // Try to obtain the map from the SupportMapFragment.
            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
                    .getMap();
            // Check if we were successful in obtaining the map.
            if (mMap != null)
            {
                setUpMap();
            }
        }
    }

    private void setUpMap()
    {
        mMap.addMarker(new MarkerOptions().position(new LatLng(0, 0)).title("Marker"));
    }

    protected void onPause()
    {
        super.onPause();
        RemoveListeners();
    }

    protected void onResume()
    {
        super.onResume();
        StartListeners();
    }

    private void StartListeners()
    {
        if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
        {
            return;//this was added for the permission check
        }
        this.lm.requestLocationUpdates("network", 0, 0.0f, this.LocationLSNR);
        this.lm.requestLocationUpdates("gps", 0, 0.0f, this.LocationLSNR);
        this.lm.addGpsStatusListener(this.GPSstatusLSNR);
    }

    private void RemoveListeners()
    {
        if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
        {
            return;//this was added for the permission check
        }
        this.lm.removeUpdates(this.LocationLSNR);
        this.lm.removeGpsStatusListener(this.GPSstatusLSNR);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_maps);
        setUpMapIfNeeded();

        this.prov = (TextView) findViewById(R.id.MyProvider);
        this.coords = (EditText) findViewById(R.id.Coords);
        this.LocationLSNR = new LocationListener()
        {
            public void onStatusChanged(String provider, int status, Bundle extras)
            { }
            public void onProviderEnabled(String provider)
            { }
            public void onProviderDisabled(String provider)
            { }

            public void onLocationChanged(final Location location)
            {
                    new Thread(new Runnable()
                    {
                        public void run()
                        {
                            MapsActivity.this.lp = MapsActivity.this.lm.getBestProvider(MapsActivity.this.criteria, true);
                            final String lpDisp = MapsActivity.this.lp.toUpperCase();
                            if (!MapsActivity.this.lp.equals("gps"))
                            {
                                MapsActivity.this.Acuracy = "Estimated";
                                MapsActivity.this.Col = -65536;
                            }
                            else if (!MapsActivity.this.GPSsignal || MapsActivity.this.WeakSignal)
                            {
                                MapsActivity.this.Acuracy = "Weak Signal";
                                MapsActivity.this.Col = -65536;
                            }
                            else
                            {
                                MapsActivity.this.Acuracy = "Signal OK";
                                MapsActivity.this.Col = -16711936;
                            }
                            String DisplayText = "";

                            MapsActivity.this.UpdateLocation(location);
                            if (MapsActivity.this.CurrentLocation != null)
                            {
                                MapsActivity.this.runOnUiThread(new Runnable()
                                {
                                    public void run()
                                    {
                                        MapsActivity.this.button.setText(MapsActivity.this.OutText);
                                        MapsActivity.this.prov.setText(String.format("Using %s (%s) Accurate to: %sm", new Object[]{lpDisp, MapsActivity.this.Acuracy, String.valueOf(MapsActivity.this.CurrentLocation.getAccuracy())}));
                                        MapsActivity.this.prov.setTextColor(MapsActivity.this.Col);
                                        int Lat = (int) MapsActivity.this.CurrentLocation.getAltitude();
                                        Lat -= Lat % 10;
                                        int Speed = (int) (((double) MapsActivity.this.CurrentLocation.getSpeed()) * 3.6d);
                                        if (!MapsActivity.this.coords.hasSelection())
                                        {
                                            MapsActivity.this.coords.setText(String.format("Latitude : %s ; Longitude : %s \n Altitude : %s m ; Speed : %s km/h", new Object[]{Double.valueOf(MapsActivity.this.CurrentLocation.getLatitude()), Double.valueOf(MapsActivity.this.CurrentLocation.getLongitude()), Integer.valueOf(Lat), Integer.valueOf(Speed)}));
                                        }
                                    }
                                });
                            }
                        }
                    }).start();
            }            
        };

        this.GPSstatusLSNR = new GpsStatus.Listener()
        {
            public void onGpsStatusChanged(int event)
            {
                switch (event)
                {
                    case 3:
                        MapsActivity.this.isFirstLock = true;
                        return;
                    case 4:
                        if (MapsActivity.this.CurrentLocation != null)
                        {
                            MapsActivity.this.isFirstLock = SystemClock.elapsedRealtime() - MapsActivity.this.mLastLocationMillis < 8000;
                            if (MapsActivity.this.isFirstLock)
                            {
                                MapsActivity.this.WeakSignal = false;
                                return;
                            }
                            MapsActivity.this.prov.setText(String.format("Using %s (%s) Accurate to: %sm", new Object[]{MapsActivity.this.lp.toUpperCase(), "Weak Signal", Float.valueOf(MapsActivity.this.CurrentLocation.getAccuracy())}));
                            MapsActivity.this.prov.setTextColor(0xffff0000);
                            return;
                        }
                        return;
                    default:
                        return;
                }
            }
        };

        this.geocoder = new Geocoder(this);
        this.lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        this.criteria = new Criteria();
        this.criteria.setAccuracy(1);
        this.lp = this.lm.getBestProvider(this.criteria, true);
        String lpDisp = this.lp.toUpperCase();
        String Acuracy = "";

        if (this.lp.equals("gps"))
        {
            Acuracy = "Initializing.....";
            this.prov.setTextColor(0xff00ff00);
        }
        else
        {
            Acuracy = "Estimated";
            this.prov.setTextColor(0xffff0000);
        }

        this.prov.setText(String.format("Using %s (%s)", new Object[]{lpDisp, Acuracy}));
        this.button.setText("Searching for location...");
        this.coords.setText("");
    }

    protected void UpdateLocation(Location location)
    {
        if (location != null)
        {
            try
            {
                this.CurrentLocation = location;
                this.mLastLocationMillis = SystemClock.elapsedRealtime();
                Address address = this.geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 5).get(0);
                if (address != null)
                {
                    this.CurrentAddress = address;
                    StringBuilder deviceLocation = new StringBuilder();
                    for (int i = 0; i <= address.getMaxAddressLineIndex(); i++)
                    {
                        deviceLocation.append(address.getAddressLine(i)).append(",\n");
                    }
                    this.OutText = deviceLocation.toString();
                    return;
                }
                this.OutText = "Unable to find address";
                return;
            }
            catch (Exception e)
            {
                if (e.getLocalizedMessage().equals("Unable to parse response from server"))
                {
                    this.OutText = "Enable Internet connection";
                    return;
                } else if (!AllertAlreadyShown)
                {
                    runOnUiThread(new Runnable()
                    {
                        public void run()
                        {
                            new AlertDialog.Builder(MapsActivity.this).setTitle("Service Error").setMessage("Some location services on your device are stopped. In order to use application you need to restart the device.").setPositiveButton("OK", null).show();
                        }
                    });
                    AllertAlreadyShown = true;
                    this.OutText = "Service not started";
                    return;
                } else {
                    return;
                }
            }
        }
        this.OutText = "Unable to find your location";
    }

}
