package com.yserri.go_tag;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by YserriCom4980 on 2/23/2016.
 */
public class Splash extends AppCompatActivity
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);

        int secondsDelayed = 1;
        new Handler().postDelayed(new Runnable()
        {
            public void run()
            {
                startActivity(new Intent(Splash.this, com.yserri.go_tag.MapsActivity.class));
                finish();
            }
        },
                secondsDelayed * 3000);

    }
}